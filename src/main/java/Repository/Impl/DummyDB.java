package Repository.Impl;

import java.util.*;
import domain.*;

public class DummyDB {
	
	public List<Person> persons;
	public List<User> users;
	public List<Address> addresses;
	public List<Meal> meals;
	public List<Component> components;
	public List<Order> orders;
	public List<Category> categories;


	
	public DummyDB() {
		persons = new ArrayList<Person>();
		users = new ArrayList<User>();
		addresses = new ArrayList<Address>();
		meals = new ArrayList<Meal>();
		components = new ArrayList<Component>();
		orders = new ArrayList<Order>();
		categories = new ArrayList<Category>();

	}
	
}
