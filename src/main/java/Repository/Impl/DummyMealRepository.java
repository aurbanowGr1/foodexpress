package Repository.Impl;

import java.util.List;

import domain.Category;
import domain.Meal;
import repositories.IMealRepository;


public class DummyMealRepository implements IMealRepository {

	private DummyDB db;
	
	public DummyMealRepository(DummyDB db){
		super();
		this.db = db;
	}
	
	public void add(Meal entity) {
		db.meals.add(entity);
	}

	public void delete(Meal entity) {
		db.meals.remove(entity);
	}

	public void update(Meal entity) {
		
	}

	public Meal get(int id) {
		for(Meal m : db.meals)
		{
			if(m.getId()==id)
				return m;
		}
		return null;
	}

	public List<Meal> getAll() {
		return db.meals;
	}

	public List<Meal> withCategory(Category category) {
		return withCategory(category.getId());
		
	}

	public List<Meal> withCategory(String categoryName) {
		Category category = null;
		for(Category c : db.categories)
		{
			if(c.getName().equalsIgnoreCase(categoryName))
			{
				category = c;
				break;
			}
		}
		if(category==null)return null;
		return category.getMeal();
	}

	public List<Meal> withCategory(int categoryId) {
		Category category = null;
		for(Category c : db.categories)
		{
			if(c.getId()==categoryId)
			{
				category = c;
				break;
			}
		}
		if(category==null)return null;
		return category.getMeal();
	}

	
	
	
}
