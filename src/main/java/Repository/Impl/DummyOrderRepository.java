package Repository.Impl;

import java.util.List;

import domain.Order;
import domain.User;
import repositories.IOrderRepository;


public class DummyOrderRepository implements IOrderRepository {
	
	private DummyDB db;
	
	public DummyOrderRepository(DummyDB db){
		super();
		this.db = db;
	}
	
	public void add(Order entity) {
		db.orders.add(entity);
	}

	public void delete(Order entity) {
		db.orders.remove(entity);
		
	}

	public void update(Order entity) {
		
	}

	public Order get(int id) {
		for(Order o : db.orders)
		{
			if(o.getId()==id)
				return o;
		}
		return null;
	}

	public List<Order> getAll() {
		return db.orders;
	}

	public List<Order> withStatus(String status) {
		return null;
	}

	public List<Order> withUser(User user) {
		return withUser(user.getId());
	}

	public List<Order> withUser(int userId) {
		User user = null;
		for(User u : db.users)
		{
			if(u.getId()==userId)
			{
				user = u;
				break;
			}
		}
		if(user==null)return null;
		return user.getOrder();
	}

}
