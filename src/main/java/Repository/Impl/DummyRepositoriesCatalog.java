package Repository.Impl;

import repositories.IRepository;
import repositories.IRepositoryCatalog;
import domain.Address;
import domain.Category;
import domain.Component;
import domain.Order;
import domain.Meal;
import domain.Person;
import domain.User;

public class DummyRepositoriesCatalog implements IRepositoryCatalog {

	private DummyDB db;
	


	public IRepository<Order> getOrders() {
		return new DummyOrderRepository(db);
	}

	public IRepository<Meal> getMeals() {
		return new DummyMealRepository(db);
	}
	public IRepository<Category> getCategories() {
		return null;
	}
	public IRepository<Person> getPersons() {
		// TODO Auto-generated method stub
		return null;
	}

	public IRepository<Address> getAddresses() {
		// TODO Auto-generated method stub
		return null;
	}

	public IRepository<User> getUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	public IRepository<Component> getComponents() {
		// TODO Auto-generated method stub
		return null;
	}



}