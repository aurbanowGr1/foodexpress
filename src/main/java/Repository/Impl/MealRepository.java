package Repository.Impl;

import java.sql.Connection;
import java.sql.SQLException;

import unitofwork.IUnitOfWork;
import domain.Meal;
import entitybuilders.IEntityBuilder;

public class MealRepository extends RepositoryBase<Meal> {

	public MealRepository(Connection connection,
			IEntityBuilder<Meal> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}

	@Override
	protected void prepareUpdateQuery(Meal entity) throws SQLException {
		update.setString(1, entity.getName());
		update.setDouble(2, entity.getPrice());
		update.setString(1, entity.getDescription());
		update.setBoolean(2, entity.getChange());
	}

	@Override
	protected void prepareAddQuery(Meal entity) throws SQLException {
		save.setString(1, entity.getName());
		save.setDouble(2, entity.getPrice());
		save.setString(1, entity.getDescription());
		save.setBoolean(2, entity.getChange());
	}

	@Override
	protected String getTableName() {
		return "meal";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE order SET (date,comment)=(?,?) WHERE id=?";
	}

	@Override
	protected String getCreateQuery() {
		return "INSERT INTO order(name,price, description, change) VALUES (?,?,?,?)";

	}

}
