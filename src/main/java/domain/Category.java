package domain;


import java.util.ArrayList;
import java.util.List;

public class Category extends Entity {
	private int id;
	private String name;
	private String description;
	private List<Meal> meal = new ArrayList<Meal>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Meal> getMeal() {
		return meal;
	}
	public void setMeal(List<Meal> meal) {
		this.meal = meal;
	}
	

}
