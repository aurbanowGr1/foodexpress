package domain;

import java.util.ArrayList;
import java.util.List;

public class Meal extends Entity {
	private int id;
	private String name;
	private double price;
	private String description;
	private Boolean change; //Możliwość edycji dania
	private Category category;
	private List<Component> component = new ArrayList<Component>(); //Produkty
	
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getChange() {
		return change;
	}
	public void setChange(Boolean change) {
		this.change = change;
	}
	public List<Component> getComponent() {
		return component;
	}
	public void setComponent(List<Component> component) {
		this.component = component;
	}
	
	

	
}
