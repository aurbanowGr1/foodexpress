package domain;

import java.util.ArrayList;
import java.util.List;

public class Person extends Entity {
	private int id;
	private String name;
	private String surname;
	private User user;
	private List<Address> address = new ArrayList<Address>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public User getUser() {
		return user;
	}
	public void setUser_id(User user) {
		this.user = user;
	}
	public List<Address> getAddress() {
		return address;
	}
	public void setAddress(List<Address> address) {
		this.address = address;
	}
	
	
}