package entitybuilders;

import java.sql.ResultSet;

import domain.Address;

public class AddressBuilder implements IEntityBuilder<Address>{

	public Address build(ResultSet rs) {
		try {
            Address a = new Address();
            a.setId(rs.getInt("id"));
            a.setName(rs.getString("name"));
            a.setSurname(rs.getString("surname"));
            a.setCity(rs.getString("city"));
            a.setStreet(rs.getString("street"));
            a.setPostCode(rs.getString("postCode"));
            a.setHouseNumber(rs.getString("houseNumber"));
            a.setPhoneNumber(rs.getString("phoneNumber"));


            return a;
        } catch (Exception ex) {
            ex.printStackTrace();
        }		return null;
	}

}
