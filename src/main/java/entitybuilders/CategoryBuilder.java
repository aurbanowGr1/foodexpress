package entitybuilders;

import java.sql.ResultSet;

import domain.Category;

public class CategoryBuilder implements IEntityBuilder<Category>{

	public Category build(ResultSet rs) {
		try {
            Category c = new Category();
            c.setId(rs.getInt("id"));
            c.setName(rs.getString("name"));
            c.setDescription(rs.getString("description"));
            return c;
        } catch (Exception ex) {
            ex.printStackTrace();
        }		return null;
	}

}
