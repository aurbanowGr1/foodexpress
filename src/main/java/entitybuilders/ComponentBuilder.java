package entitybuilders;

import java.sql.ResultSet;

import domain.Component;

public class ComponentBuilder implements IEntityBuilder<Component>{

	public Component build(ResultSet rs) {
		try {
            Component c = new Component();
            c.setId(rs.getInt("id"));
            c.setName(rs.getString("name"));
            c.setPrice(rs.getDouble("price"));
            return c;
        } catch (Exception ex) {
            ex.printStackTrace();
        }		return null;
	}

}
