package entitybuilders;

import java.sql.ResultSet;

import domain.Meal;

public class MealBuilder implements IEntityBuilder<Meal>{

	public Meal build(ResultSet rs) {
		try {
            Meal m = new Meal();
            m.setId(rs.getInt("id"));
            m.setName(rs.getString("name"));
            m.setDescription(rs.getString("description"));
            m.setPrice(rs.getDouble("price"));
            return m;
        } catch (Exception ex) {
            ex.printStackTrace();
        }		return null;
	}

}
