package entitybuilders;

import java.sql.ResultSet;

import domain.Order;

public class OrderBuilder implements IEntityBuilder<Order>{

	public Order build(ResultSet rs) {
		try {
            Order o = new Order();
            o.setId(rs.getInt("id"));
            o.setDate(rs.getDate("date"));
            o.setComment(rs.getString("comment"));
            o.setStatus(rs.getString("status"));
            return o;
        } catch (Exception ex) {
            ex.printStackTrace();
        }		return null;
	}

}
