package entitybuilders;

import java.sql.ResultSet;

import domain.Person;

public class PersonBuilder implements IEntityBuilder<Person>{

	public Person build(ResultSet rs) {
		try {
            Person p = new Person();
            p.setId(rs.getInt("id"));
            p.setName(rs.getString("name"));
            p.setSurname(rs.getString("surname"));
            return p;
        } catch (Exception ex) {
            ex.printStackTrace();
        }		return null;
	}

}
