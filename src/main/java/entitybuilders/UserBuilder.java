package entitybuilders;

import java.sql.ResultSet;

import domain.User;

public class UserBuilder implements IEntityBuilder<User>{

	public User build(ResultSet rs) {
		try {
            User u = new User();
            u.setId(rs.getInt("id"));
            u.setLogin(rs.getString("login"));
            u.setPass(rs.getString("pass"));
            u.setEmail(rs.getString("email"));
            return u;
        } catch (Exception ex) {
            ex.printStackTrace();
        }		return null;
	}

}
