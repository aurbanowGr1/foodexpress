package repositories;

import java.util.List;

import domain.*;

public interface IMealRepository extends IRepository<Meal> {
	
	public List<Meal> withCategory (Category category);
	public List<Meal> withCategory (String categoryName);
	public List<Meal> withCategory (int categoryId);

	
}
