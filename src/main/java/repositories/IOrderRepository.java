package repositories;
import java.util.List;

import domain.*;

public interface IOrderRepository extends IRepository<Order> {


	public List<Order> withStatus(String status);
	public List<Order> withUser(User user);
	public List<Order> withUser(int user);


	
}

