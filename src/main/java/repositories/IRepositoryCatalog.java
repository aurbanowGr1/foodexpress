package repositories;
import domain.*;

public interface IRepositoryCatalog {

	public IRepository<Person> getPersons();
	public IRepository<Address> getAddresses();
	public IRepository<Order> getOrders();
	public IRepository<Category> getCategories();
	public IRepository<User> getUsers();
	public IRepository<Component> getComponents();
	public IRepository<Meal> getMeals();


}
