package repositories_tests;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import repositories.IRepository;
import unitofwork.IUnitOfWork;
import unitofwork.UnitOfWork;
import domain.Meal;
import domain.Person;
import entitybuilders.IEntityBuilder;
import entitybuilders.MealBuilder;
import entitybuilders.PersonBuilder;
import Repository.Impl.DummyDB;
import Repository.Impl.DummyMealRepository;
import Repository.Impl.MealRepository;
import Repository.Impl.PersonRepository;
import junit.framework.TestCase;

public class MealRepositoryTest extends TestCase {


	
	public void setUp(){
		try{
			String dbUrl = "jdbc:mysql://localhost/foodexpress";
		    String username = "root";
		    String password = "";		
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection connection = DriverManager.getConnection(dbUrl,username,password);
		    
			IEntityBuilder<Meal> builder = new MealBuilder();
			IUnitOfWork uow = new UnitOfWork(connection);
			IRepository<Meal> mealRepo = 	new MealRepository(connection,builder,uow);
			
			Meal meal1 = new Meal();
			Meal meal2 = new Meal();
			
			meal1.setId(1);
			meal1.setName("Schabowy ziemniakami");
			meal1.setChange(false);
			meal1.setDescription("Schabowy z ziemniakami i pysznym sosem");
			meal1.setPrice(25.00);
			
			meal2.setId(2);
			meal2.setName("Pizza");
			meal2.setChange(true);
			meal2.setDescription(null);
			meal2.setPrice(20.00);
			meal2.setComponent(null);
			
			mealRepo.add(meal1);
			mealRepo.add(meal2);
    		uow.commit();
    		connection.close();
    		
    	 } catch (Exception e) {
             e.printStackTrace();
         }
	}
	
	public void testGet(){
	//	Meal mealTest1 = mealRepo.get(1);
	//	assertEquals(meal1, mealTest1);
	}
	
	public void testGetAll(){
	//	List <Meal> meals = (List<Meal>) mealRepo.getAll();
	//	assertEquals(meals, db.meals);
		
	}
	
}
