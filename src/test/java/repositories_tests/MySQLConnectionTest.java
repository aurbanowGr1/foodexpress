package repositories_tests;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import repositories.IRepository;
import unitofwork.IUnitOfWork;
import unitofwork.UnitOfWork;
import Repository.Impl.PersonRepository;
import domain.Person;
import entitybuilders.IEntityBuilder;
import entitybuilders.PersonBuilder;
import junit.framework.TestCase;

public class MySQLConnectionTest extends TestCase {

    public void testConnect() {
    String dbUrl = "jdbc:mysql://localhost/foodexpress";
    String dbClass = "com.mysql.jdbc.Driver";
    String query = "Select * from person";
    String username = "root";
    String password = "";
    try {

        Class.forName(dbClass);
        Connection connection = DriverManager.getConnection(dbUrl,
            username, password);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
	        int person_id = resultSet.getInt(1);
	        String person_name = resultSet.getString(2);
	        String person_surname = resultSet.getString(3);
	        System.out.println("ID : " + person_id);
	        System.out.println("Imie : " + person_name);
	        System.out.println("Nazwisko : " + person_surname);
        }
        
        System.out.println("");

        ResultSet resultSet1 = statement.executeQuery(query);
        System.out.println("TWORZYMY NOWEGO UŻYTKOWNIKA");
        IEntityBuilder<Person> builder = new PersonBuilder();
		IUnitOfWork uow = new UnitOfWork(connection);
		IRepository<Person> repo = 	new PersonRepository(connection,builder,uow);
		Person p = new Person();
		p.setName("jan");
		p.setSurname("Nowak");
		repo.add(p);
		uow.commit();
		
        System.out.println("");
        
		while (resultSet1.next()) {
	        int person_id = resultSet1.getInt(1);
	        String person_name = resultSet1.getString(2);
	        String person_surname = resultSet1.getString(3);
	        System.out.println("ID : " + person_id);
	        System.out.println("Imie : " + person_name);
	        System.out.println("Nazwisko : " + person_surname);
	        }
      
        connection.close();
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
        System.out.println("Error1");
    } catch (SQLException e) {
        e.printStackTrace();
        System.out.println("Error2");
    }
    }
}